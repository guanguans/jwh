<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title><?php echo ($site_seo_title); ?> <?php echo ($site_name); ?></title>
    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />
    <meta name="description" content="<?php echo ($site_seo_description); ?>">
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/jiuwenhua/themes/html/Public/js/jquery.min.js"></script>
<script src="/jiuwenhua/themes/html/Public/js/jquery.SuperSlide.2.1.1.js"></script>
<link href="/jiuwenhua/themes/html/Public/css/css.css" rel="stylesheet">
</head>
<body>
<div class="header">
    <div class="phone">全国客服电话：400-999-7109</div>
    <!-- <ul>
        <li><a href="#">联系我们</a></li>
        <li><a href="#">事业机会</a></li>
        <li><a href="#">产品商城</a></li>
        <li><a href="#">新闻动态</a></li>
        <li><a href="#" id="xxoo">走进我们</a></li>
        <li><a href="#">首页</a></li>
    </ul> -->
    <?php
 $effected_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='dropdown-toggle' data-toggle='dropdown'>\$label <b class='caret'></b></a>"; $ul_class="dropdown-menu" ; $li_class="" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="nav-list">
      <a href="<?php echo leuu('page/index',array(id=>8));?>" class="first-a">公司介绍</a>
      <a href="<?php echo leuu('page/index',array(id=>9));?>">秦含章专栏</a>
      <a href="#">发展历程</a>
      <a href="<?php echo leuu('page/index',array(id=>10));?>">企业文化</a>
      <a href="<?php echo leuu('page/index',array(id=>11));?>">核心团队</a>
    </div>
     <div class="clear"></div>
</div>
<div class="slide">
    <img src="/jiuwenhua/themes/html/Public/images/slide-product.jpg" width="1200" style="border:1px solid #000" alt="公司介绍">
</div>
<div class="product-main">
   <div class="sideMenu">
      <a href="/jiuwenhua" class="myline"><h3>首页</h3></a>
      <ul style="border:0px solid #fff;"></ul>
      <a href="<?php echo leuu('page/index',array(id=>8));?>" class="over-color"><h3 class="we-show-1">走进我们</h3></a>
      <ul class="we-show-2"> 
        <a href="<?php echo leuu('page/index',array(id=>8));?>"><li>公司介绍</li></a>
        <a href="<?php echo leuu('page/index',array(id=>9));?>"><li>秦含章专栏</li></a>
        <a href="#"><li>发展历程</li></a>
        <a href="<?php echo leuu('page/index',array(id=>10));?>"><li>企业文化</li></a>
        <a href="<?php echo leuu('page/index',array(id=>11));?>"><li>核心团队</li></a>
      </ul>
      <a href="<?php echo leuu('list/index',array(id=>2));?>" class="myline"><h3>新闻动态</h3></a>     
      <a href="#" class="myline"><h3>产品商城</h3></a>   
      <a href="<?php echo leuu('list/index',array(id=>8));?>" class="myline"><h3>事业机会</h3></a>   
      <a href="<?php echo leuu('page/index',array(id=>5));?>" class="myline"><h3>联系我们</h3></a> 
  </div>
   <div class="news-right">
       <h2>公司介绍<span>Company Introduction<span><div class="line-bottom"></h2>
       <div class="product-right-content">
            <?php $id = I('id'); $page=sp_sql_page($id); ?>
            <p><?php echo ($page['post_content']); ?></p>
           <!-- <br>
           <p>
                秦含章（北京）酒文化发展有限公司即将创建的秦含章酒文化展览馆将汇聚各界名酒收藏爱好者，公司将不定期推出中国名酒品鉴及拍卖活动。公司提倡“饮健康酒、健康饮酒”。秦含章基金会成立后每年1月 9日将举办“秦含章奖”颁奖大会，以表彰在中国酒界作出突出贡献的个人及企业。基金会还将不定期举办国家品酒师资格培训等一系列活动。
            </p>
            <br>
            <p>
                中国酒文化产业园将着力打造集休闲、旅游、观光、购物、餐饮、住宿为一体的新型文化体验园，竭诚欢迎来自全球五湖四海的宾朋好友。
            </p> -->
           <div class="clear"></div>
       </div>
       <h2>公司架构<span>Company Structure<span><div class="line-bottom"></div></h2>
       <div class="product-right-content-2" style="width:100%;float:left;">
       </div>
   </div>
   <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="footer">
    <p>秦含章（北京）酒文化发展有限公司</p>
    <?php $links=sp_getlinks(); $top = array_slice($links, 0, 6); $bottom = array_slice($links, 6, 10); ?>
    <center>
    <?php if(is_array($top)): foreach($top as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
         
        <!-- <span><a href="#">山西杏花村汾酒集团有限公司</a></span>
        <span><a href="#">四川宜宾五粮液集团有限公司</a></span>
        <span><a href="#">四川泸州老窖集团有限责任公司</a></span>
        <span<a href="#">>四川全兴酒业有限公司</a></span>
        <span><a href="#">安徽古井有限责任公司</a></span> -->
    </center>
    <center>
    <?php if(is_array($bottom)): foreach($bottom as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
        <!-- <span><a href="#">贵州董酒股份有限公司</a></span>
        <span><a href="#">陕西西凤酒股份有限公司</a></span>
        <span><a href="#">中国长城葡萄酒公司</a></span>
        <span><a href="#">山西老陈醋集团</a></span> -->
    </center>
    <p class="last-p"><!-- Copyright &copy; 2010HBstars.com Incorporated. All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;-->京ICP备17003025号</p>
</div>
<script type="text/javascript">
    //导航菜单
    $("#menu-item-5 a").mouseover(function(){
        $(".nav-list").css({display:'block'});
        $(".first-a").css({color:'#fff'});  
    });
    $("#menu-item-5 a").mouseout(function(){
        $(".nav-list").css({display:'none'});
        $(".first-a").css({color:'#d4ab73'});  
    });
    $(".nav-list").mouseover(function(){
        $(".nav-list").css({display:'block'});  
    });
    $(".nav-list").mouseout(function(){
        $(".nav-list").css({display:'none'}); 
    });
    $(".first-a").mouseover(function(){
        $(".first-a").css({color:'#fff'});  
    });
     $(".first-a").mouseout(function(){
        $(".first-a").css({color:'#d4ab73'});  
    });
    //左侧栏风琴
    $(".we-show-1").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".we-show-2").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".myline").mouseover(function(){
        $(".we-show-1").css('border-bottom',"1px solid #827b79");
    });
    var ary = location.href.split("&");
    jQuery(".sideMenu").slide({ titCell:"h3", targetCell:"ul", effect:ary[1], delayTime:ary[2] , trigger:ary[4], triggerTime:ary[4], defaultPlay:ary[5], returnDefault:ary[6],easing:ary[7] });
</script>
</body>
</html>