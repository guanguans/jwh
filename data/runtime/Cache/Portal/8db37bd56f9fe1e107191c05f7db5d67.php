<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html><html lang="zh-CN"><head>    <title><?php echo ($site_seo_title); ?> <?php echo ($site_name); ?></title>    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />    <meta name="description" content="<?php echo ($site_seo_description); ?>">    <script type="text/javascript" src="/jiuwenhua/themes/html/Public/js/ckplayer/ckplayer.js"></script>    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/jiuwenhua/themes/html/Public/js/jquery.min.js"></script>
<script src="/jiuwenhua/themes/html/Public/js/jquery.SuperSlide.2.1.1.js"></script>
<link href="/jiuwenhua/themes/html/Public/css/css.css" rel="stylesheet"></head><body><div class="header">
    <div class="phone">全国客服电话：400-999-7109</div>
    <!-- <ul>
        <li><a href="#">联系我们</a></li>
        <li><a href="#">事业机会</a></li>
        <li><a href="#">产品商城</a></li>
        <li><a href="#">新闻动态</a></li>
        <li><a href="#" id="xxoo">走进我们</a></li>
        <li><a href="#">首页</a></li>
    </ul> -->
    <?php
 $effected_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='dropdown-toggle' data-toggle='dropdown'>\$label <b class='caret'></b></a>"; $ul_class="dropdown-menu" ; $li_class="" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="nav-list">
      <a href="<?php echo leuu('page/index',array(id=>8));?>" class="first-a">公司介绍</a>
      <a href="<?php echo leuu('page/index',array(id=>9));?>">秦含章专栏</a>
      <a href="#">发展历程</a>
      <a href="<?php echo leuu('page/index',array(id=>10));?>">企业文化</a>
      <a href="<?php echo leuu('page/index',array(id=>11));?>">核心团队</a>
    </div>
     <div class="clear"></div>
</div><div class="slide">    <img src="/jiuwenhua/themes/html/Public/images/slide.png" alt=""></div><div class="main">    <div class="column">        <div class="column-nav">            <a href="<?php echo leuu('page/index',array(id=>9));?>">            <span>秦含章专栏</span> <span>|</span>            <span>Special Column</span>            </a>        </div>        <div class="line"></div>        <div class="column-content">            <a href="<?php echo leuu('page/index',array(id=>9));?>">                <img src="/jiuwenhua/themes/html/Public/images/qhz.jpg" alt="秦含章">                <ul style="padding-right:10px;box-sizing:border-box;">                    <li>中国食品与发酵工业奠基人</li>                    <li>酒界泰斗</li>                    <li>中国食品工业专业协会</li>                    <li>名誉会长</li>                    <li>中国食品发酵工业研究所</li>                    <li>名誉所长</li>                    <li>中国工程院院士候选人评审委员会</li>                    <li>委员</li>                    <li>秦含章基金会</li>                    <li>荣誉会长</li>                    <li>秦含章（北京）酒文化发展有限公司终身荣誉顾问</li>                </ul>                <p style="padding-right:10px;box-sizing:border-box;text-indent:2em;">中国著名食品工业科学家和工程技术家1908年2月出生，江苏无锡人。1931年毕业于上海国立劳动大学农学院，后赴比利时、法国、德国留学。圣布律高等农......</p>                <div class="clear"></div>            </a>            </div>    </div>    <div class="news">        <div class="news-nav">            <a href="<?php echo leuu('list/index',array(id=>2));?>">                <span>新闻动态</span>                 <span>|</span>                <span>News</span>            </a>        </div>        <div class="line"></div>        <?php $lists = sp_sql_posts_paged("cid:2;order:post_date DESC;",5); ?>        <ul>            <?php if(is_array($lists['posts'])): foreach($lists['posts'] as $key=>$vo): ?><a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>"><li><?php echo ($vo['post_title']); ?></li></a><?php endforeach; endif; ?>        </ul>    </div>    <div class="witness">        <div class="witness-nav">			<a href="<?php echo leuu('page/index',array(id=>9));?>">				<span>荣耀见证</span>				<span>|</span>				<span>Glory Witness</span>			</a>        </div>        <div class="line"></div>         <ul>			<a href="<?php echo leuu('page/index',array(id=>9));?>">				<img src="/jiuwenhua/themes/html/Public/images/witness-content3.jpg" alt="荣耀见证" width="111">				<img src="/jiuwenhua/themes/html/Public/images/witness-content4.jpg" alt="荣耀见证" width="111">				<img src="/jiuwenhua/themes/html/Public/images/witness-content5.jpg" alt="荣耀见证" width="111">				<img src="/jiuwenhua/themes/html/Public/images/witness-content6.jpg" alt="荣耀见证" width="111">			</a>        </ul>    </div>    <div class="video" style="position:relative;">        <div class="video-nav">            <span>宣传视频</span>            <span>|</span>            <span>Promotional Video</span>        </div>        <div class="line"></div>        <div class="product-video" id="a1" style="margin-top:20px;"></div>        <div style="width:230px;height:20px; position:absolute; top:100px; left:0px;color:#fff;text-align:center;">秦含章纪录片-《大国匠心 中国酒魂》</div>        <div class="product-video" id="a2" style="margin-top:5px;"></div>        <div style="width:230px;height:20px; position:absolute; top:280px; left:0px;color:#fff;text-align:center;">秦含章先生109寿辰庆典 精彩留念</div>    </div>    <div class="clear"></div></div><div class="footer">
    <p>秦含章（北京）酒文化发展有限公司</p>
    <?php $links=sp_getlinks(); $top = array_slice($links, 0, 6); $bottom = array_slice($links, 6, 10); ?>
    <center>
    <?php if(is_array($top)): foreach($top as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
         
        <!-- <span><a href="#">山西杏花村汾酒集团有限公司</a></span>
        <span><a href="#">四川宜宾五粮液集团有限公司</a></span>
        <span><a href="#">四川泸州老窖集团有限责任公司</a></span>
        <span<a href="#">>四川全兴酒业有限公司</a></span>
        <span><a href="#">安徽古井有限责任公司</a></span> -->
    </center>
    <center>
    <?php if(is_array($bottom)): foreach($bottom as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
        <!-- <span><a href="#">贵州董酒股份有限公司</a></span>
        <span><a href="#">陕西西凤酒股份有限公司</a></span>
        <span><a href="#">中国长城葡萄酒公司</a></span>
        <span><a href="#">山西老陈醋集团</a></span> -->
    </center>
    <p class="last-p"><!-- Copyright &copy; 2010HBstars.com Incorporated. All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;-->京ICP备17003025号</p>
</div>
<script type="text/javascript">
    //导航菜单
    $("#menu-item-5 a").mouseover(function(){
        $(".nav-list").css({display:'block'});
        $(".first-a").css({color:'#fff'});  
    });
    $("#menu-item-5 a").mouseout(function(){
        $(".nav-list").css({display:'none'});
        $(".first-a").css({color:'#d4ab73'});  
    });
    $(".nav-list").mouseover(function(){
        $(".nav-list").css({display:'block'});  
    });
    $(".nav-list").mouseout(function(){
        $(".nav-list").css({display:'none'}); 
    });
    $(".first-a").mouseover(function(){
        $(".first-a").css({color:'#fff'});  
    });
     $(".first-a").mouseout(function(){
        $(".first-a").css({color:'#d4ab73'});  
    });
    //左侧栏风琴
    $(".we-show-1").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".we-show-2").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".myline").mouseover(function(){
        $(".we-show-1").css('border-bottom',"1px solid #827b79");
    });
    var ary = location.href.split("&");
    jQuery(".sideMenu").slide({ titCell:"h3", targetCell:"ul", effect:ary[1], delayTime:ary[2] , trigger:ary[4], triggerTime:ary[4], defaultPlay:ary[5], returnDefault:ary[6],easing:ary[7] });
</script></body><script type="text/javascript">    var flashvars={        f:"/jiuwenhua/themes/html/Public/video/video_bottom.mp4",        c:0,//是否读取文本配置,0不是，1是  去logo、广告        p:0,//视频默认0是暂停，1是播放，2是不加载视频        e:1,//视频结束后的动作，0是调用js函数，1是循环播放，2是暂停播放并且不调用广告        i:'/jiuwenhua/themes/html/Public/images/video-bottom.png',//初始图片地址    };    var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};    CKobject.embedSWF('/jiuwenhua/themes/html/Public/js/ckplayer/ckplayer.swf','a1','ckplayer_a1','230','175',flashvars,params);     var flashvars={        f:"/jiuwenhua/themes/html/Public/video/video_top.mp4",        c:0,        e:1,        i:'/jiuwenhua/themes/html/Public/images/video-top.png',    };    var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};    CKobject.embedSWF('/jiuwenhua/themes/html/Public/js/ckplayer/ckplayer.swf','a2','ckplayer_a2','230','175',flashvars,params);</script></html>