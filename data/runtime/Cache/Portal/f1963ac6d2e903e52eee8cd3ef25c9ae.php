<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title><?php echo ($site_seo_title); ?> <?php echo ($site_name); ?></title>
    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />
    <meta name="description" content="<?php echo ($site_seo_description); ?>">
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/jiuwenhua/themes/html/Public/js/jquery.min.js"></script>
<script src="/jiuwenhua/themes/html/Public/js/jquery.SuperSlide.2.1.1.js"></script>
<link href="/jiuwenhua/themes/html/Public/css/css.css" rel="stylesheet">
</head>
<body>
<div class="header">
    <div class="phone">全国客服电话：400-999-7109</div>
    <!-- <ul>
        <li><a href="#">联系我们</a></li>
        <li><a href="#">事业机会</a></li>
        <li><a href="#">产品商城</a></li>
        <li><a href="#">新闻动态</a></li>
        <li><a href="#" id="xxoo">走进我们</a></li>
        <li><a href="#">首页</a></li>
    </ul> -->
    <?php
 $effected_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='dropdown-toggle' data-toggle='dropdown'>\$label <b class='caret'></b></a>"; $ul_class="dropdown-menu" ; $li_class="" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="nav-list">
      <a href="<?php echo leuu('page/index',array(id=>8));?>" class="first-a">公司介绍</a>
      <a href="<?php echo leuu('page/index',array(id=>9));?>">秦含章专栏</a>
      <a href="#">发展历程</a>
      <a href="<?php echo leuu('page/index',array(id=>10));?>">企业文化</a>
      <a href="<?php echo leuu('page/index',array(id=>11));?>">核心团队</a>
    </div>
     <div class="clear"></div>
</div>
<div class="slide">
    <img src="/jiuwenhua/themes/html/Public/images/news-slide.jpg" alt="">
</div>
<div class="qhz-main">
   <div class="sideMenu" >
      <a href="/jiuwenhua" class="myline"><h3>首页</h3></a>
      <ul style="border:0px solid #fff;"></ul>
      <a href="<?php echo leuu('page/index',array(id=>8));?>"><h3 class="we-show-1">走进我们</h3></a>
      <ul class="we-show-2"> 
        <a href="<?php echo leuu('page/index',array(id=>8));?>"><li>公司介绍</li></a>
        <a href="<?php echo leuu('page/index',array(id=>9));?>"><li>秦含章专栏</li></a>
        <a href="#"><li>发展历程</li></a>
        <a href="<?php echo leuu('page/index',array(id=>10));?>"><li>企业文化</li></a>
        <a href="<?php echo leuu('page/index',array(id=>11));?>"><li>核心团队</li></a>
      </ul>
      <a href="<?php echo leuu('list/index',array(id=>2));?>" class="myline"><h3>新闻动态</h3></a>     
      <a href="#" class="myline"><h3>产品商城</h3></a>   
      <a href="<?php echo leuu('list/index',array(id=>8));?>" class="myline"><h3>事业机会</h3></a>   
      <a href="<?php echo leuu('page/index',array(id=>5));?>" class="myline"><h3>联系我们</h3></a>   
  </div>
   <?php $id = I('id'); $page=sp_sql_page($id); $smeta=json_decode($page['smeta'], true); ?>
   <div class="term-right">
       <h2><?php echo ($page['post_title']); ?><span>Core Term<span><div class="line-bottom"></div></h2>
       <div class="term-right-content">
            <div class="qhz-right-content-photo" >
                <img src="/jiuwenhua/themes/html/Public/images/term-1.jpg" alt="核心团队人物">
                <div><span>秦立新</span>董事长</div>
                <div>秦含章先生之孙</div>
                
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-2.jpg" alt="核心团队人物">
                <div><span>杨刚</span>总经理</div>
                <div>天使投资人 </div>
                <div>万众易货（北京）国际投资有限公司<span>总裁</span></div>
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-3.jpg" alt="核心团队人物">
                <div><span>王小龙</span>副总裁</div>
                <div>NGO项目管理人  复转军人</div>
                <div>外资企业十五年行政管理经验</div>
                <div>秦含章（北京）酒文化发展有限公司<span>监事</span></div>
                <div>万众易货（北京）国际投资有限公司<span>董事</span></div>
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-4.jpg" alt="核心团队人物">
                <div><span>杨 丹</span>副总裁</div>
                <div>法学学士   经济学硕士   金融理财师</div>
                <div>基金管理公司十余年从业经历</div>
                <div>秦含章（北京）酒文化发展有限公司<span>董事</span></div>
                <div>万众易货（北京）国际投资有限公司<span>董事</span></div>
                <div class="clear"></div>
            </div>


            <div class="qhz-right-content-photo" style="margin-right:300px;margin-left:330px;">
                <img src="/jiuwenhua/themes/html/Public/images/term-5.jpg" alt="核心团队人物">
                <div><span>秦含章</span>终身荣誉顾问</div>
                <div>中国食品与发酵工业奠基人<span>酒界泰斗</span></div>
                <div>中国食品工业专业协会<span>名誉会长</span></div>
                <div>中国食品发酵工业研究所 <span>名誉所长</span></div>
                <div>中国工程院院士候选人评审委员会<span>委员</span></div>
                <div>万众易货（北京）国际投资有限公司<span>董事</span></div>
                <div class="clear"></div>
            </div>

            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-6.jpg" alt="核心团队人物">
                <div><span>沈怡方</span>荣誉顾问</div>
                <div>中国著名白酒专家</div>
                <div>教授级高级工程师</div>
                <div>中国第三届评酒会<span>委员</span></div>
                <div>中国食品工业协会白酒专业协会<span>副会长</span></div>
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-7.jpg" alt="核心团队人物">
                <div><span>高景炎</span>荣誉顾问</div>
                <div>原北京酿酒总厂<span>厂长</span></div>
                <div>白酒专家委员会<span>主任委员</span></div>
                <div>中清酿造技艺发展中心<span>副理事长</span></div>
                <div>中国食品工业协会白酒专业委员会<span>副会长</span></div>
                <div>二锅头酒第八代传承人</div>
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-8.jpg" alt="核心团队人物">
                <div><span>季克良</span>荣誉顾问</div>
                <div>原贵州茅台集团<span>董事长</span></div>
                <div>原贵州茅台酒厂有限公司<span>董事长</span></div>
                <div>世界级酿造大师<span>著名白酒专家</span></div>
                <div>五一劳动奖章获得者<span>全国劳动模范</span></div>
                <div>茅台非物质文化遗产传承人</div>
                <div class="clear"></div>
            </div>
            <div class="qhz-right-content-photo">
                <img src="/jiuwenhua/themes/html/Public/images/term-9.jpg" alt="核心团队人物">
                <div><span>徐研 </span>荣誉顾问</div>
                <div>江南大学副校长</div>
                <div>中国酒业协会副理事长</div>
                <div>酿酒科学与酶技术中心主任</div>
                <div>中国酒业协会白酒技术委员会副主任</div>
                <div>我国第一位白酒博士</div>
                <div class="clear"></div>
            </div>
           <div class="clear"></div>
       </div>
   </div>
   <div class="clear"></div>
</div>
<div class="footer">
    <p>秦含章（北京）酒文化发展有限公司</p>
    <?php $links=sp_getlinks(); $top = array_slice($links, 0, 6); $bottom = array_slice($links, 6, 10); ?>
    <center>
    <?php if(is_array($top)): foreach($top as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
         
        <!-- <span><a href="#">山西杏花村汾酒集团有限公司</a></span>
        <span><a href="#">四川宜宾五粮液集团有限公司</a></span>
        <span><a href="#">四川泸州老窖集团有限责任公司</a></span>
        <span<a href="#">>四川全兴酒业有限公司</a></span>
        <span><a href="#">安徽古井有限责任公司</a></span> -->
    </center>
    <center>
    <?php if(is_array($bottom)): foreach($bottom as $key=>$vo): ?><span style="line-height:30px;"><a href="<?php echo ($vo["link_url"]); ?>" target="<?php echo ($vo["link_target"]); ?>"><?php echo ($vo["link_name"]); ?></a></span><?php endforeach; endif; ?>
        <!-- <span><a href="#">贵州董酒股份有限公司</a></span>
        <span><a href="#">陕西西凤酒股份有限公司</a></span>
        <span><a href="#">中国长城葡萄酒公司</a></span>
        <span><a href="#">山西老陈醋集团</a></span> -->
    </center>
    <p class="last-p"><!-- Copyright &copy; 2010HBstars.com Incorporated. All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;-->京ICP备17003025号</p>
</div>
<script type="text/javascript">
    //导航菜单
    $("#menu-item-5 a").mouseover(function(){
        $(".nav-list").css({display:'block'});
        $(".first-a").css({color:'#fff'});  
    });
    $("#menu-item-5 a").mouseout(function(){
        $(".nav-list").css({display:'none'});
        $(".first-a").css({color:'#d4ab73'});  
    });
    $(".nav-list").mouseover(function(){
        $(".nav-list").css({display:'block'});  
    });
    $(".nav-list").mouseout(function(){
        $(".nav-list").css({display:'none'}); 
    });
    $(".first-a").mouseover(function(){
        $(".first-a").css({color:'#fff'});  
    });
     $(".first-a").mouseout(function(){
        $(".first-a").css({color:'#d4ab73'});  
    });
    //左侧栏风琴
    $(".we-show-1").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".we-show-2").mouseover(function(){
        $(".we-show-1").css({border:"0px solid #fff"});
    });
    $(".myline").mouseover(function(){
        $(".we-show-1").css('border-bottom',"1px solid #827b79");
    });
    var ary = location.href.split("&");
    jQuery(".sideMenu").slide({ titCell:"h3", targetCell:"ul", effect:ary[1], delayTime:ary[2] , trigger:ary[4], triggerTime:ary[4], defaultPlay:ary[5], returnDefault:ary[6],easing:ary[7] });
</script>
</body>
</html>